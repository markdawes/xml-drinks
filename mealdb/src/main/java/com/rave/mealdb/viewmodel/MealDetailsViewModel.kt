package com.rave.mealdb.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.mealdb.model.MealRepo
import com.rave.mealdb.model.local.FullMeal
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * View model for meal details screen.
 *
 * @property repo
 * @constructor Create empty Meal details view model
 */
@HiltViewModel
class MealDetailsViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {
    private var _meal: MutableLiveData<FullMeal> = MutableLiveData()
    val meal: LiveData<FullMeal> get() = _meal

    /**
     * Fetch full meal details from id.
     *
     * @param id
     */
    fun fetchMealFromId(id: String) = viewModelScope.launch {
        _meal.value = repo.getMealFromId(id)
        println(meal.value)
    }
}
