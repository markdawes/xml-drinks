package com.rave.mealdb.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.mealdb.model.MealRepo
import com.rave.mealdb.model.local.Meal
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * View model for meal list screen.
 *
 * @property repo
 * @constructor Create empty Meal list view model
 */
@HiltViewModel
class MealListViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {
    private var _meals: MutableLiveData<List<Meal>> = MutableLiveData()
    val meals: LiveData<List<Meal>> get() = _meals

    /**
     * Fetch meals from category name.
     *
     * @param category
     */
    fun fetchMealsFromCategory(category: String) = viewModelScope.launch {
        _meals.value = repo.getMealsFromCategory(category)
    }

    /**
     * Fetch meals from area name.
     *
     * @param area
     */
    fun fetchMealsFromArea(area: String) = viewModelScope.launch {
        _meals.value = repo.getMealsFromArea(area)
    }

    /**
     * Fetch meals from ingredient name.
     *
     * @param ingredient
     */
    fun fetchMealsFromIngredient(ingredient: String) = viewModelScope.launch {
        _meals.value = repo.getMealsFromIngredient(ingredient)
    }
}
