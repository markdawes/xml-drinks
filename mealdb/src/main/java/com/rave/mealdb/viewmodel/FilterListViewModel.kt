package com.rave.mealdb.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.mealdb.model.MealRepo
import com.rave.mealdb.model.local.Area
import com.rave.mealdb.model.local.Category
import com.rave.mealdb.model.local.Ingredient
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * View model for filter list screen.
 *
 * @property repo
 * @constructor Create empty filter list view model
 */
@HiltViewModel
class FilterListViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {
    private var _categories: MutableLiveData<List<Category>> = MutableLiveData()
    val categories: LiveData<List<Category>> get() = _categories

    private var _areas: MutableLiveData<List<Area>> = MutableLiveData()
    val areas: LiveData<List<Area>> get() = _areas

    private var _ingredients: MutableLiveData<List<Ingredient>> = MutableLiveData()
    val ingredients: LiveData<List<Ingredient>> get() = _ingredients

    init {
        fetchCategories()
        fetchAreas()
        fetchIngredients()
    }

    /**
     * Fetch categories from repo.
     *
     */
    private fun fetchCategories() = viewModelScope.launch {
        _categories.value = repo.getMealCategories()
    }

    /**
     * Fetch areas from repo.
     *
     */
    private fun fetchAreas() = viewModelScope.launch {
        _areas.value = repo.getMealAreas()
    }

    /**
     * Fetch ingredients from repo.
     *
     */
    private fun fetchIngredients() = viewModelScope.launch {
        _ingredients.value = repo.getMealIngredients()
    }
}
