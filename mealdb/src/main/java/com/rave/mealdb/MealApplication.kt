package com.rave.mealdb

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Custom application class for hilt.
 *
 * @constructor Create empty Soccer application
 */
@HiltAndroidApp
class MealApplication : Application()
