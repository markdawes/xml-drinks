package com.rave.mealdb.model.local

/**
 * Area class.
 *
 * @property strArea
 * @constructor Create empty Area d t o
 */
data class Area(
    val strArea: String
)
