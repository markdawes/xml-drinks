package com.rave.mealdb.model.remote

import com.rave.mealdb.model.remote.dto.AreaResponse
import com.rave.mealdb.model.remote.dto.CategoryResponse
import com.rave.mealdb.model.remote.dto.FullMealResponse
import com.rave.mealdb.model.remote.dto.IngredientResponse
import com.rave.mealdb.model.remote.dto.MealResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Service class that creates the endpoints needed.
 *
 * @constructor Create empty Api service
 */
interface APIService {

    @GET(LIST_ENDPOINT)
    suspend fun getMealCategories(@Query("c") arg: String = "list"): CategoryResponse

    @GET(LIST_ENDPOINT)
    suspend fun getMealAreas(@Query("a") arg: String = "list"): AreaResponse

    @GET(LIST_ENDPOINT)
    suspend fun getMealIngredients(@Query("i") arg: String = "list"): IngredientResponse

    @GET(FILTER_ENDPOINT)
    suspend fun getMealsFromCategory(@Query("c") name: String): MealResponse

    @GET(FILTER_ENDPOINT)
    suspend fun getMealsFromArea(@Query("a") name: String): MealResponse

    @GET(FILTER_ENDPOINT)
    suspend fun getMealsFromIngredient(@Query("i") name: String): MealResponse

    @GET(LOOKUP_ENDPOINT)
    suspend fun getMealFromId(@Query("i") id: String): FullMealResponse

    companion object {
        private const val LIST_ENDPOINT = "list.php"
        private const val FILTER_ENDPOINT = "filter.php"
        private const val LOOKUP_ENDPOINT = "lookup.php"
    }
}
