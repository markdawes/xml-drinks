package com.rave.mealdb.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class AreaDTO(
    val strArea: String
)
