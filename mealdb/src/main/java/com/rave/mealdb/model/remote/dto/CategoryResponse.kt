package com.rave.mealdb.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class CategoryResponse(
    val meals: List<CategoryDTO>
)
