package com.rave.mealdb.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class IngredientDTO(
    val idIngredient: String,
    val strDescription: String?,
    val strIngredient: String,
    val strType: String?
)
