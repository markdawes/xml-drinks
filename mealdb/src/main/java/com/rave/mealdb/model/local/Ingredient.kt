package com.rave.mealdb.model.local

/**
 * Ingredient.
 *
 * @property idIngredient
 * @property strDescription
 * @property strIngredient
 * @property strType
 * @constructor Create empty Ingredient
 */
data class Ingredient(
    val idIngredient: String,
    val strDescription: String?,
    val strIngredient: String,
    val strType: String?
)
