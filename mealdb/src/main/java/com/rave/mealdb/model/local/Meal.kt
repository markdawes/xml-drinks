package com.rave.mealdb.model.local

/**
 * Meal.
 *
 * @property idMeal
 * @property strMeal
 * @property strMealThumb
 * @constructor Create empty Meal
 */
data class Meal(
    val idMeal: String,
    val strMeal: String,
    val strMealThumb: String
)
