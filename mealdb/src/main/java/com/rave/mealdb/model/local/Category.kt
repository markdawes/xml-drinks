package com.rave.mealdb.model.local

/**
 * Category.
 *
 * @property strCategory
 * @constructor Create empty Category d t o
 */
data class Category(
    val strCategory: String
)
