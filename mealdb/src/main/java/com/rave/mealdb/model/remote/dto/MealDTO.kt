package com.rave.mealdb.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class MealDTO(
    val idMeal: String,
    val strMeal: String,
    val strMealThumb: String
)
