package com.rave.mealdb.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class FullMealResponse(
    val meals: List<FullMealDTO>
)
