package com.rave.mealdb.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class AreaResponse(
    val meals: List<AreaDTO>
)
