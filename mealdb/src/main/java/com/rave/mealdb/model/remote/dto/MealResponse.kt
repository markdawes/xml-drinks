package com.rave.mealdb.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class MealResponse(
    val meals: List<MealDTO>
)
