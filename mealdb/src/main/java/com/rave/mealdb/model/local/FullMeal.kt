package com.rave.mealdb.model.local

/**
 * class for full meal details.
 *
 * @property idMeal
 * @property strIngredient1
 * @property strIngredient2
 * @property strIngredient3
 * @property strIngredient4
 * @property strInstructions
 * @property strMeal
 * @property strMealThumb
 * @constructor Create empty Full meal
 */
data class FullMeal(
    val idMeal: String?,
    val strIngredient1: String?,
    val strIngredient2: String?,
    val strIngredient3: String?,
    val strIngredient4: String?,
    val strInstructions: String?,
    val strMeal: String?,
    val strMealThumb: String?
)
