package com.rave.mealdb.model

import com.rave.mealdb.model.local.Area
import com.rave.mealdb.model.local.Category
import com.rave.mealdb.model.local.FullMeal
import com.rave.mealdb.model.local.Ingredient
import com.rave.mealdb.model.local.Meal
import com.rave.mealdb.model.remote.APIService
import javax.inject.Inject

/**
 * Repository class to mediate how meal data is fetched.
 *
 * @property service
 * @constructor Create empty Drink repo
 */
class MealRepo @Inject constructor(private val service: APIService) {

    /**
     * Fetch meal categories.
     *
     * @return
     */
    suspend fun getMealCategories(): List<Category> {
        val categoryDTOs = service.getMealCategories().meals
        return categoryDTOs!!.map {
            Category(
                strCategory = it.strCategory
            )
        }
    }

    /**
     * Fetch meal areas.
     *
     * @return
     */
    suspend fun getMealAreas(): List<Area> {
        val areaDTOs = service.getMealAreas().meals
        return areaDTOs!!.map {
            Area(
                strArea = it.strArea
            )
        }
    }

    /**
     * Fetch meal ingredients.
     *
     * @return
     */
    suspend fun getMealIngredients(): List<Ingredient> {
        val ingredientDTOs = service.getMealIngredients().meals
        return ingredientDTOs!!.map {
            Ingredient(
                idIngredient = it.idIngredient,
                strDescription = it.strDescription,
                strIngredient = it.strIngredient,
                strType = it.strType
            )
        }
    }

    /**
     * Get meals from category name.
     *
     * @param category
     * @return
     */
    suspend fun getMealsFromCategory(category: String): List<Meal> {
        val mealDTOs = service.getMealsFromCategory(category).meals
        return mealDTOs!!.map {
            Meal(
                idMeal = it.idMeal,
                strMeal = it.strMeal,
                strMealThumb = it.strMealThumb
            )
        }
    }

    /**
     * Get meals from area name.
     *
     * @param area
     * @return
     */
    suspend fun getMealsFromArea(area: String): List<Meal> {
        val mealDTOs = service.getMealsFromArea(area).meals
        return mealDTOs!!.map {
            Meal(
                idMeal = it.idMeal,
                strMeal = it.strMeal,
                strMealThumb = it.strMealThumb
            )
        }
    }

    /**
     * Get meals from ingredient name.
     *
     * @param ingredient
     * @return
     */
    suspend fun getMealsFromIngredient(ingredient: String): List<Meal> {
        val mealDTOs = service.getMealsFromIngredient(ingredient).meals
        return mealDTOs!!.map {
            Meal(
                idMeal = it.idMeal,
                strMeal = it.strMeal,
                strMealThumb = it.strMealThumb
            )
        }
    }

    /**
     * Get full meal details with id.
     *
     * @param id
     * @return
     */
    suspend fun getMealFromId(id: String): FullMeal {
        val mealDTO = service.getMealFromId(id).meals
        return FullMeal(
            idMeal = mealDTO[0].idMeal,
            strMeal = mealDTO[0].strMeal,
            strMealThumb = mealDTO[0].strMealThumb,
            strInstructions = mealDTO[0].strInstructions,
            strIngredient1 = mealDTO[0].strIngredient1,
            strIngredient2 = mealDTO[0].strIngredient2,
            strIngredient3 = mealDTO[0].strIngredient3,
            strIngredient4 = mealDTO[0].strIngredient4
        )
    }
}
