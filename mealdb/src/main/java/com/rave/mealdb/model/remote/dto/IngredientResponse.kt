package com.rave.mealdb.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class IngredientResponse(
    val meals: List<IngredientDTO>
)
