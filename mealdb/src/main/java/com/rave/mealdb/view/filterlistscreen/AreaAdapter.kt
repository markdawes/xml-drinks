package com.rave.mealdb.view.filterlistscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.rave.mealdb.R
import com.rave.mealdb.model.local.Area
import kotlinx.android.synthetic.main.item_category.view.tvCategory

/**
 * Class to set area data to recyclerview.
 *
 * @constructor Create empty Category adapter
 */
class AreaAdapter : RecyclerView.Adapter<AreaAdapter.AreaViewHolder>() {

    private var areaList = emptyList<Area>()

    /**
     * Area view holder.
     *
     * @constructor
     *
     * @param itemView
     */
    inner class AreaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AreaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return AreaViewHolder(view)
    }

    override fun onBindViewHolder(holder: AreaViewHolder, position: Int) {
        val currentItem = areaList[position]
        holder.itemView.tvCategory.text = currentItem.strArea
        holder.itemView.tvCategory.setOnClickListener {
            val args = bundleOf(
                "title" to currentItem.strArea,
                "type" to "a"
            )
            Navigation.findNavController(holder.itemView).navigate(
                R.id.action_filterListFragment_to_mealListFragment,
                args
            )
        }
    }

    override fun getItemCount(): Int {
        return areaList.size
    }

    /**
     * Set data.
     *
     * @param area
     */
    fun setData(area: List<Area>) {
        this.areaList = area
        notifyDataSetChanged()
    }
}
