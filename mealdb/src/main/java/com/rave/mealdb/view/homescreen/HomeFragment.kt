package com.rave.mealdb.view.homescreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rave.mealdb.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.view.btnArea
import kotlinx.android.synthetic.main.fragment_home.view.btnCategories
import kotlinx.android.synthetic.main.fragment_home.view.btnIngredients

/**
 * Fragment for the home screen.
 */
@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val title = "title"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_home, container, false)

        view.btnCategories.setOnClickListener {
            val args = bundleOf(title to "Categories")
            findNavController().navigate(R.id.action_homeFragment_to_filterListFragment, args)
        }

        view.btnArea.setOnClickListener {
            val args = bundleOf(title to "Areas")
            findNavController().navigate(R.id.action_homeFragment_to_filterListFragment, args)
        }

        view.btnIngredients.setOnClickListener {
            val args = bundleOf(title to "Ingredients")
            findNavController().navigate(R.id.action_homeFragment_to_filterListFragment, args)
        }

        return view
    }
}
