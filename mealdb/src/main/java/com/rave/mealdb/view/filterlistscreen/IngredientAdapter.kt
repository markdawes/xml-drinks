package com.rave.mealdb.view.filterlistscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.rave.mealdb.R
import com.rave.mealdb.model.local.Ingredient
import kotlinx.android.synthetic.main.item_category.view.tvCategory

/**
 * Class to set ingredient data to recyclerview.
 *
 * @constructor Create empty Category adapter
 */
class IngredientAdapter : RecyclerView.Adapter<IngredientAdapter.IngredientViewHolder>() {

    private var ingredientList = emptyList<Ingredient>()

    /**
     * Ingredient view holder.
     *
     * @constructor
     *
     * @param itemView
     */
    inner class IngredientViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return IngredientViewHolder(view)
    }

    override fun onBindViewHolder(holder: IngredientViewHolder, position: Int) {
        val currentItem = ingredientList[position]
        holder.itemView.tvCategory.text = currentItem.strIngredient
        holder.itemView.tvCategory.setOnClickListener {
            val args = bundleOf(
                "title" to currentItem.strIngredient,
                "type" to "i"
            )
            Navigation.findNavController(holder.itemView).navigate(
                R.id.action_filterListFragment_to_mealListFragment,
                args
            )
        }
    }

    override fun getItemCount(): Int {
        return ingredientList.size
    }

    /**
     * Set data.
     *
     * @param ingredient
     */
    fun setData(ingredient: List<Ingredient>) {
        this.ingredientList = ingredient
        notifyDataSetChanged()
    }
}
