package com.rave.mealdb.view.mealdetailsscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.rave.mealdb.R
import com.rave.mealdb.viewmodel.MealDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_meal_details.view.ivPicture
import kotlinx.android.synthetic.main.fragment_meal_details.view.tvIngredients
import kotlinx.android.synthetic.main.fragment_meal_details.view.tvInstructions
import kotlinx.android.synthetic.main.fragment_meal_details.view.tvTitle

/**
 * Fragment for displaying full meal details.
 */
@AndroidEntryPoint
class MealDetailsFragment : Fragment() {

    private val id by lazy { arguments?.getString("id") }

    private val viewModel by viewModels<MealDetailsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_meal_details, container, false)
        viewModel.fetchMealFromId(id!!)

        view.tvTitle.text = viewModel.meal.value?.strMeal
        Glide.with(view)
            .load(viewModel.meal.value?.strMealThumb)
            .into(view.ivPicture)

        @Suppress("MaxLineLength")
        view.tvIngredients.text = "${viewModel.meal.value?.strIngredient1}\n${viewModel.meal.value?.strIngredient2}\n${viewModel.meal.value?.strIngredient3}\n${viewModel.meal.value?.strIngredient4}"
        view.tvInstructions.text = viewModel.meal.value?.strInstructions

        return view
    }
}
