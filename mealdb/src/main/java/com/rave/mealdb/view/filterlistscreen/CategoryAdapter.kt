package com.rave.mealdb.view.filterlistscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.rave.mealdb.R
import com.rave.mealdb.model.local.Category
import kotlinx.android.synthetic.main.item_category.view.tvCategory

/**
 * Class to set category data to recyclerview.
 *
 * @constructor Create empty Category adapter
 */
class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categoryList = emptyList<Category>()

    /**
     * Category view holder.
     *
     * @constructor
     *
     * @param itemView
     */
    inner class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return CategoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val currentItem = categoryList[position]
        holder.itemView.tvCategory.text = currentItem.strCategory
        holder.itemView.tvCategory.setOnClickListener {
            val args = bundleOf(
                "title" to currentItem.strCategory,
                "type" to "c"
            )
            Navigation.findNavController(holder.itemView).navigate(
                R.id.action_filterListFragment_to_mealListFragment,
                args
            )
        }
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    /**
     * Set data.
     *
     * @param category
     */
    fun setData(category: List<Category>) {
        this.categoryList = category
        notifyDataSetChanged()
    }
}
