package com.rave.mealdb.view.meallistscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rave.mealdb.R
import com.rave.mealdb.model.local.Meal
import kotlinx.android.synthetic.main.item_meal.view.ivMeal
import kotlinx.android.synthetic.main.item_meal.view.tvMeal

/**
 * Class to set meal data to recyclerview.
 *
 * @constructor Create empty Category adapter
 */
class MealAdapter : RecyclerView.Adapter<MealAdapter.MealViewHolder>() {

    private var mealList = emptyList<Meal>()

    /**
     * Area view holder.
     *
     * @constructor
     *
     * @param itemView
     */
    inner class MealViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_meal, parent, false)
        return MealViewHolder(view)
    }

    override fun onBindViewHolder(holder: MealViewHolder, position: Int) {
        val currentItem = mealList[position]
        holder.itemView.tvMeal.text = currentItem.strMeal
        Glide.with(holder.itemView)
            .load(currentItem.strMealThumb)
            .into(holder.itemView.ivMeal)
        holder.itemView.rootView.setOnClickListener {
            val args = bundleOf(
                "title" to currentItem.strMeal,
                "id" to currentItem.idMeal
            )
            Navigation.findNavController(holder.itemView).navigate(
                R.id.action_mealListFragment_to_mealDetailsFragment,
                args
            )
        }
    }

    override fun getItemCount(): Int {
        return mealList.size
    }

    /**
     * Set data.
     *
     * @param meal
     */
    fun setData(meal: List<Meal>) {
        this.mealList = meal
        notifyDataSetChanged()
    }
}
