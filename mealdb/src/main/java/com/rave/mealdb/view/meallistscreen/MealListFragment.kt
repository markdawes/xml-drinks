package com.rave.mealdb.view.meallistscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.mealdb.R
import com.rave.mealdb.viewmodel.MealListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_filter_list.view.rvList
import kotlinx.android.synthetic.main.fragment_meal_list.view.tvTitle

/**
 * Fragment for the meal list screen.
 */
@AndroidEntryPoint
class MealListFragment : Fragment() {

    private val title by lazy { arguments?.getString("title") }
    private val type by lazy { arguments?.getString("type") }

    private val viewModel by viewModels<MealListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_meal_list, container, false)

        when (type) {
            "c" -> viewModel.fetchMealsFromCategory(title!!)
            "a" -> viewModel.fetchMealsFromArea(title!!)
            "i" -> viewModel.fetchMealsFromIngredient(title!!)
        }

        view.tvTitle.text = title
        val adapter = MealAdapter()
        viewModel.meals.observe(
            viewLifecycleOwner,
            Observer {
                adapter.setData(it)
            }
        )
        view.rvList.adapter = adapter
        view.rvList.layoutManager = LinearLayoutManager(requireContext())

        return view
    }
}
