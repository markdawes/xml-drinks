package com.rave.mealdb.view.filterlistscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.mealdb.R
import com.rave.mealdb.viewmodel.FilterListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_filter_list.view.rvList
import kotlinx.android.synthetic.main.fragment_filter_list.view.tvFilter

/**
 * A fragment for the filter list screen.
 */
@AndroidEntryPoint
class FilterListFragment : Fragment() {

    private val title by lazy { arguments?.getString("title") }

    private val viewModel by viewModels<FilterListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_filter_list, container, false)

        view.tvFilter.text = title

        if (title == "Categories") {
            val adapter = CategoryAdapter()
            viewModel.categories.observe(
                viewLifecycleOwner,
                Observer {
                    adapter.setData(it)
                }
            )
            view.rvList.adapter = adapter
            view.rvList.layoutManager = LinearLayoutManager(requireContext())
        } else if (title == "Areas") {
            val adapter = AreaAdapter()
            viewModel.areas.observe(
                viewLifecycleOwner,
                Observer {
                    adapter.setData(it)
                }
            )
            view.rvList.adapter = adapter
            view.rvList.layoutManager = LinearLayoutManager(requireContext())
        } else if (title == "Ingredients") {
            val adapter = IngredientAdapter()
            viewModel.ingredients.observe(
                viewLifecycleOwner,
                Observer {
                    adapter.setData(it)
                }
            )
            view.rvList.adapter = adapter
            view.rvList.layoutManager = LinearLayoutManager(requireContext())
        }

        return view
    }
}
