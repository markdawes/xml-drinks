package com.rave.xmldrinks.viewmodel

import com.rave.xmldrinks.model.DrinkRepo
import com.rave.xmldrinks.model.local.Category
import com.rave.xmldrinks.util.CoroutinesTestExtension
import com.rave.xmldrinks.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
internal class ViewModelTest {

    @RegisterExtension
    val testExtension = CoroutinesTestExtension()

    private val repo: DrinkRepo = mockk()
    private val viewModel: CategoryViewModel = CategoryViewModel(repo)

    @Test
    fun testInitialValue() = runTest(testExtension.dispatcher) {
        // Given

        // When
        val categories = viewModel.categories.value

        // Then
        Assertions.assertTrue(categories.isNullOrEmpty())
    }

    @Test
    fun testValueChange() = runTest(testExtension.dispatcher) {
        val expectedValue = listOf(Category(strCategory = "Gin"))
        // Given
        coEvery { repo.getDrinkCategories() } coAnswers { expectedValue }

        // When
        viewModel.fetchCategories()

        // Then
        val categories = viewModel.categories.value
        Assertions.assertTrue(categories?.size == 1)
        Assertions.assertEquals(categories, expectedValue)
    }
}
