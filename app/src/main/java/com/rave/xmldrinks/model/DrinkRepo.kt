package com.rave.xmldrinks.model

import com.rave.xmldrinks.model.local.Category
import com.rave.xmldrinks.model.remote.APIService
import javax.inject.Inject

/**
 * Repository class to mediate how drink data is fetched.
 *
 * @property service
 * @constructor Create empty Drink repo
 */
class DrinkRepo @Inject constructor(private val service: APIService) {

    /**
     * Fetch drink categories.
     *
     * @return
     */
    suspend fun getDrinkCategories(): List<Category> {
        val categoryDTOs = service.getAllDrinkCategories().drinks
        return categoryDTOs!!.map {
            Category(
                strCategory = it.strCategory
            )
        }
    }
}
