package com.rave.xmldrinks.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class CategoryResponse(
    val drinks: List<CategoryDTO> = emptyList()
)
