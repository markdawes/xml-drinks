package com.rave.xmldrinks.model.remote

import com.rave.xmldrinks.model.remote.dto.CategoryResponse
import retrofit2.http.GET

/**
 * Service class that creates the endpoints needed.
 *
 * @constructor Create empty Api service
 */
interface APIService {

    @GET(CATEGORY_ENDPOINT)
    suspend fun getAllDrinkCategories(): CategoryResponse

    companion object {
        private const val CATEGORY_ENDPOINT = "list.php?c=list"
    }
}
