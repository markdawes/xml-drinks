package com.rave.xmldrinks.model.local

/**
 * Category.
 *
 * @property strCategory
 * @constructor Create empty Category
 */
data class Category(
    val strCategory: String
)
