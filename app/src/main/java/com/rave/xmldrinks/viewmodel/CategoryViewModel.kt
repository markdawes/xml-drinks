package com.rave.xmldrinks.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.xmldrinks.model.DrinkRepo
import com.rave.xmldrinks.model.local.Category
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Main view model.
 *
 * @property repo
 * @constructor Create empty Drink view model
 */
@HiltViewModel
class CategoryViewModel @Inject constructor(private val repo: DrinkRepo) : ViewModel() {
    private var _categories: MutableLiveData<List<Category>> = MutableLiveData()
    val categories: LiveData<List<Category>> get() = _categories

    /**
     * Fetch categories from repo.
     *
     */
    fun fetchCategories() = viewModelScope.launch {
        _categories.value = repo.getDrinkCategories()
    }
}
