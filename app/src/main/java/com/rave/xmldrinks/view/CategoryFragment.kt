package com.rave.xmldrinks.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.xmldrinks.databinding.FragmentCategoryBinding
import com.rave.xmldrinks.viewmodel.CategoryViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Screen to display list of categories.
 */
@AndroidEntryPoint
class CategoryFragment : Fragment() {

    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<CategoryViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentCategoryBinding.inflate(inflater, container, false).apply {
            _binding = this

            viewModel.fetchCategories()
            val adapter = CategoryAdapter()
            viewModel.categories.observe(
                viewLifecycleOwner,
                Observer {
                    adapter.setData(it)
                }
            )
            binding.rvCategories.adapter = adapter
            binding.rvCategories.layoutManager = LinearLayoutManager(requireContext())
        }.root
    }
}
