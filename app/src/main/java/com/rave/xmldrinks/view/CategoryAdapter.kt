package com.rave.xmldrinks.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.xmldrinks.databinding.ItemCategoryBinding
import com.rave.xmldrinks.model.local.Category

/**
 * Cless to set category data to recyclerview.
 *
 * @constructor Create empty Category adapter
 */
class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categoryList = emptyList<Category>()

    /**
     * Category view holder.
     *
     * @property binding
     * @constructor Create empty Category view holder
     */
    inner class CategoryViewHolder(val binding: ItemCategoryBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Display category.
         *
         * @param category
         */
        fun displayCategory(category: Category) = with(binding) {
            tvCategory.text = category.strCategory
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        println("on create called")
        return CategoryViewHolder(
            ItemCategoryBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.displayCategory(categoryList[position])
        println("on bind called")
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    /**
     * Set data.
     *
     * @param category
     */
    fun setData(category: List<Category>) {
        this.categoryList = category
        notifyDataSetChanged()
    }
}
